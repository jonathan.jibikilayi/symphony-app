<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* voiture/voitures.html.twig */
class __TwigTemplate_67b8d6d430af3524c96654be776e625a17db858c33b35e89cdabe23686cf34b4 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'monTitre' => [$this, 'block_monTitre'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "voiture/voitures.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "voiture/voitures.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "voiture/voitures.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Liste des voitures";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_monTitre($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "monTitre"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "monTitre"));

        echo "Liste des voitures";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 7, $this->source); })()), "flashes", [0 => "success"], "method", false, false, false, 7));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 8
            echo "    <div class=\"alert alert-success\">
        ";
            // line 9
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
    </div>
";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
";
        // line 13
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 13, $this->source); })()), 'form_start');
        echo "
<div class=\"row no-gutters align-items-center text-center\">
    <div class=\"col\">
        ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), "minAnnee", [], "any", false, false, false, 16), 'row');
        echo "
    </div>
    <div class=\"col\">
        ";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 19, $this->source); })()), "maxAnnee", [], "any", false, false, false, 19), 'row');
        echo "
    </div>
    <div class=\"col-2\">
        <input type=\"submit\" value=\"rechercher\" class=\"btn btn-info\">
    </div>
</div>
    
";
        // line 26
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 26, $this->source); })()), 'form_end');
        echo "
";
        // line 27
        if ((isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new RuntimeError('Variable "admin" does not exist.', 27, $this->source); })())) {
            // line 28
            echo "    <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("creationVoiture");
            echo "\" class=\"d-block btn btn-success\">Ajouter</a>
";
        }
        // line 30
        echo "<div class=\"row no-gutters\">
    ";
        // line 31
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["voitures"]) || array_key_exists("voitures", $context) ? $context["voitures"] : (function () { throw new RuntimeError('Variable "voitures" does not exist.', 31, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["voiture"]) {
            // line 32
            echo "        <div class=\"col-12 col-lg-6 p-1\">
            <div class=\"card mb-3\">
                <div class=\"row no-gutters align-items-center p-2\">
                    <div class=\"col-12 col-md-4\">
                        <img src=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(("images/" . twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["voiture"], "modele", [], "any", false, false, false, 36), "image", [], "any", false, false, false, 36))), "html", null, true);
            echo "\" class=\"card-img\">
                    </div>
                    <div class=\"col\">
                        <div class=\"card-body\">
                            <h5 class=\"card-title\">";
            // line 40
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["voiture"], "immatriculation", [], "any", false, false, false, 40), "html", null, true);
            echo "</h5>
                            <p class=\"card-text\">
                                <div>Marque : ";
            // line 42
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["voiture"], "modele", [], "any", false, false, false, 42), "marque", [], "any", false, false, false, 42), "libelle", [], "any", false, false, false, 42), "html", null, true);
            echo "</div>
                                <div>Modele : ";
            // line 43
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["voiture"], "modele", [], "any", false, false, false, 43), "libelle", [], "any", false, false, false, 43), "html", null, true);
            echo " - PM : ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["voiture"], "modele", [], "any", false, false, false, 43), "prixMoyen", [], "any", false, false, false, 43), "html", null, true);
            echo "</div>
                                <div>Nombre de porte : ";
            // line 44
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["voiture"], "nbPortes", [], "any", false, false, false, 44), "html", null, true);
            echo "</div>
                                <div>Année : ";
            // line 45
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["voiture"], "annee", [], "any", false, false, false, 45), "html", null, true);
            echo "</div>
                            </p>
                        </div>
                    </div>
                </div>
                ";
            // line 50
            if ((isset($context["admin"]) || array_key_exists("admin", $context) ? $context["admin"] : (function () { throw new RuntimeError('Variable "admin" does not exist.', 50, $this->source); })())) {
                // line 51
                echo "                    <div class=\"row no-gutters\">
                        <a href=\"";
                // line 52
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("modifVoiture", ["id" => twig_get_attribute($this->env, $this->source, $context["voiture"], "id", [], "any", false, false, false, 52)]), "html", null, true);
                echo "\" class=\"col btn btn-warning\">Modifier</a>
                        <form method=\"post\" class=\"col\" action=\"";
                // line 53
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("supVoiture", ["id" => twig_get_attribute($this->env, $this->source, $context["voiture"], "id", [], "any", false, false, false, 53)]), "html", null, true);
                echo "\" onsubmit=\"return confirm('Voulez-vous vraiment supprimer ?');\">
                            <input type=\"hidden\" name=\"_method\" value=\"SUP\">
                            <input type=\"hidden\" name=\"_token\" value=\"";
                // line 55
                echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken(("SUP" . twig_get_attribute($this->env, $this->source, $context["voiture"], "id", [], "any", false, false, false, 55))), "html", null, true);
                echo "\">
                            <input type=\"submit\" class=\"btn btn-danger w-100\" value=\"supprimer\">
                        </form>
                    </div>
                ";
            }
            // line 60
            echo "            </div>
        </div>
       
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['voiture'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "</div>
<div class=\"navigation\">
    ";
        // line 66
        echo $this->extensions['Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension']->render($this->env, (isset($context["voitures"]) || array_key_exists("voitures", $context) ? $context["voitures"] : (function () { throw new RuntimeError('Variable "voitures" does not exist.', 66, $this->source); })()));
        echo "
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "voiture/voitures.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  243 => 66,  239 => 64,  230 => 60,  222 => 55,  217 => 53,  213 => 52,  210 => 51,  208 => 50,  200 => 45,  196 => 44,  190 => 43,  186 => 42,  181 => 40,  174 => 36,  168 => 32,  164 => 31,  161 => 30,  155 => 28,  153 => 27,  149 => 26,  139 => 19,  133 => 16,  127 => 13,  124 => 12,  115 => 9,  112 => 8,  108 => 7,  98 => 6,  79 => 5,  60 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Liste des voitures{% endblock %}

{% block monTitre %}Liste des voitures{% endblock %}
{% block body %}
{% for message in app.flashes('success') %}
    <div class=\"alert alert-success\">
        {{message}}
    </div>
{% endfor %}

{{form_start(form)}}
<div class=\"row no-gutters align-items-center text-center\">
    <div class=\"col\">
        {{form_row(form.minAnnee)}}
    </div>
    <div class=\"col\">
        {{form_row(form.maxAnnee)}}
    </div>
    <div class=\"col-2\">
        <input type=\"submit\" value=\"rechercher\" class=\"btn btn-info\">
    </div>
</div>
    
{{form_end(form)}}
{% if admin %}
    <a href=\"{{path('creationVoiture')}}\" class=\"d-block btn btn-success\">Ajouter</a>
{% endif %}
<div class=\"row no-gutters\">
    {% for voiture in voitures %}
        <div class=\"col-12 col-lg-6 p-1\">
            <div class=\"card mb-3\">
                <div class=\"row no-gutters align-items-center p-2\">
                    <div class=\"col-12 col-md-4\">
                        <img src=\"{{asset('images/' ~ voiture.modele.image)}}\" class=\"card-img\">
                    </div>
                    <div class=\"col\">
                        <div class=\"card-body\">
                            <h5 class=\"card-title\">{{voiture.immatriculation}}</h5>
                            <p class=\"card-text\">
                                <div>Marque : {{voiture.modele.marque.libelle}}</div>
                                <div>Modele : {{voiture.modele.libelle}} - PM : {{voiture.modele.prixMoyen}}</div>
                                <div>Nombre de porte : {{voiture.nbPortes}}</div>
                                <div>Année : {{voiture.annee}}</div>
                            </p>
                        </div>
                    </div>
                </div>
                {% if admin %}
                    <div class=\"row no-gutters\">
                        <a href=\"{{path('modifVoiture',{'id' : voiture.id})}}\" class=\"col btn btn-warning\">Modifier</a>
                        <form method=\"post\" class=\"col\" action=\"{{path('supVoiture',{'id':voiture.id})}}\" onsubmit=\"return confirm('Voulez-vous vraiment supprimer ?');\">
                            <input type=\"hidden\" name=\"_method\" value=\"SUP\">
                            <input type=\"hidden\" name=\"_token\" value=\"{{csrf_token('SUP' ~ voiture.id)}}\">
                            <input type=\"submit\" class=\"btn btn-danger w-100\" value=\"supprimer\">
                        </form>
                    </div>
                {% endif %}
            </div>
        </div>
       
    {% endfor %}
</div>
<div class=\"navigation\">
    {{ knp_pagination_render(voitures) }}
</div>

{% endblock %}
", "voiture/voitures.html.twig", "D:\\H2PROG\\Cours\\13-Symfony\\Realisations\\4-voitures\\templates\\voiture\\voitures.html.twig");
    }
}
