<?php

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.
// Returns the private '.errored..service_locator.ZMYYoRV.App\Entity\Voiture' shared service.

include_once $this->targetDirs[3].'\\src\\Entity\\Voiture.php';

return $this->privates['.errored..service_locator.ZMYYoRV.App\\Entity\\Voiture'] = new \App\Entity\Voiture();
